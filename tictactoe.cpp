#include <iostream>
#include <vector>
#include <SDL2/SDL.h>
//#include <SDL2/SDL_timer.h>
#include <limits.h>

const int WINDOW_WIDTH = 320;
const int WINDOW_HEIGHT = 320;
int mouseX, mouseY;

enum {EMPTY, PLAYER, COMPUTER};
short array[9] = { EMPTY };
short computerMove;

bool turn = false;
bool won = false;

void drawFrame(SDL_Renderer* renderer)
{
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
    SDL_RenderDrawLine(renderer, 110, 10, 110, 310);
    SDL_RenderDrawLine(renderer, 210, 10, 210, 310);
    SDL_RenderDrawLine(renderer, 10, 110, 310, 110);
    SDL_RenderDrawLine(renderer, 10, 210, 310, 210);
}

void drawX(SDL_Renderer* renderer, short i)
{
    int xOffset = i % 3 * 100;
    int yOffset = i / 3 * 100;

    SDL_RenderDrawLine(renderer, 20 + xOffset, 20 + yOffset, 100 + xOffset, 90 + yOffset);
    SDL_RenderDrawLine(renderer, 20 + xOffset, 100 + yOffset, 100 + xOffset, 20 + yOffset);
}

void plotCirclePoints(SDL_Renderer* renderer,int x, int y, int centerX, int centerY)
{
	SDL_RenderDrawPoint(renderer, centerX + x, centerY + y);
	SDL_RenderDrawPoint(renderer, centerX + x, centerY - y);
	SDL_RenderDrawPoint(renderer, centerX - x, centerY + y);
	SDL_RenderDrawPoint(renderer, centerX - x, centerY - y);
	SDL_RenderDrawPoint(renderer, centerX + y, centerY + x);
	SDL_RenderDrawPoint(renderer, centerX - y, centerY + x);
	SDL_RenderDrawPoint(renderer, centerX + y, centerY - x);
	SDL_RenderDrawPoint(renderer, centerX - y, centerY - x);
}

//Draws a new raster circle using Bresenham's algorithm
void drawCircle(SDL_Renderer* renderer, short i) 
{
	int currentY = 40;
    int centerX = i % 3 * 100 + 60;
    int centerY = i / 3 * 100 + 60;
	int d = 1 - 4 * 40;//Go only one eighth of a circle

	for (int currentX = 0; currentX < ceil(40/sqrt(2)); currentX++)
	{
		plotCirclePoints(renderer, currentX, currentY, centerX, centerY);
		d += 8*currentX+ 4;
		if(d>0)
		{      
			d += 8 - 8*currentY;
			--currentY;
		}
	}
}

void drawO(SDL_Renderer* renderer,short i)
{
    drawCircle(renderer, i);
}

bool checkForWin(SDL_Renderer* renderer, int player)
{
    //horizontal
    if(array[0] == array[1] && array[1] == array[2] && array[0] == player)
    {
        SDL_RenderDrawLine(renderer, 30, 60, 280, 60);
        return true;
    }

    if(array[3] == array[4] && array[4] == array[5] && array[3] == player)
    {
        SDL_RenderDrawLine(renderer, 30, 160, 280, 160);
        return true;
    }

    if(array[6] == array[7] && array[7] == array[8] && array[6] == player)
    {
        SDL_RenderDrawLine(renderer, 30, 260, 280, 260);
        return true;
    }

    //vertical
    if(array[0] == array[3] && array[3] == array[6] && array[0] == player)
    {
        SDL_RenderDrawLine(renderer, 60, 30, 60, 300);
        return true;
    }

    if(array[1] == array[4] && array[4] == array[7] && array[1] == player)
    {
        SDL_RenderDrawLine(renderer, 160, 30, 160, 300);
        return true;
    }

    if(array[2] == array[5] && array[5] == array[8] && array[2] == player)
    {
        SDL_RenderDrawLine(renderer, 260, 30, 260, 300);
        return true;
    }

    //diagonal
    if(array[0] == array[4] && array[4] == array[8] && array[0] == player)
    {
        SDL_RenderDrawLine(renderer, 30, 30, 300, 300);
        return true;
    }

    if(array[2] == array[4] && array[4] == array[6] && array[2] == player)
    {
        SDL_RenderDrawLine(renderer, 30, 300, 300, 30);
        return true;
    }

    return false;
}

void drawGame(SDL_Renderer* renderer)
{
    drawFrame(renderer);

    for(short i = 0; i< 9; i++)
    {
        switch(array[i])
        {
            case PLAYER: 
                drawX(renderer, i);
            break;

            case COMPUTER:
                drawO(renderer, i);
            break;

            default:
            break;
        }
    }

    won = checkForWin(renderer, PLAYER) || checkForWin(renderer, COMPUTER);
}

int computeScore(bool currentTurn)
{
    if(checkForWin(NULL, COMPUTER))
    {
        return 1;
    }

    if(checkForWin(NULL, PLAYER))
    {
        return -1;
    }

    std::vector<int> scores;
    std::vector<short> moves;

    for(short i = 0; i < 9; i++)
    {
        if(array[i] == EMPTY)
        {
            if(currentTurn)
                array[i] = COMPUTER;
            else
                array[i] = PLAYER;
            
            scores.push_back(computeScore(!currentTurn));
            moves.push_back(i);

            array[i] = EMPTY;
        }
    }

    if(scores.size() == 0)
        return 0;

    int score;
    short move;

    if(currentTurn)
    {
        score = -2;
        for(unsigned int i = 0; i < scores.size(); i++)
        {
            if(scores.at(i) > score)
            {
                score = scores.at(i);
                move = moves.at(i);
            }
        }
    }
    else
    {
        score = 2;
        for(unsigned int i = 0; i < scores.size(); i++)
        {
            if(scores.at(i) < score)
            {
                score = scores.at(i);
                move = moves.at(i);
            }
        }
    }
    
    computerMove = move;
    return score;
}

void makeMove()
{
    computeScore(true);

    array[computerMove] = COMPUTER;
}

int main()
{
  
    if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        std::cout << "Failed to initialize SDL! Error:" << SDL_GetError() << std::endl;
        exit(1);
    }

    //create window
    SDL_Window *window = NULL;
    window = SDL_CreateWindow("TTT", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH,WINDOW_HEIGHT,SDL_WINDOW_SHOWN);
    if(window == NULL)
    {
        std::cout << "Failed to create window! Error:" << SDL_GetError() << std::endl;
        exit(1);
    }

    //create and color window surface
    SDL_Surface *surface = NULL;
    surface = SDL_GetWindowSurface(window);
    SDL_FillRect(surface, NULL,SDL_MapRGB(surface->format, 0x00, 0x00, 0x00));

    //create renderer
    SDL_Renderer *renderer = NULL;
    renderer = SDL_GetRenderer(window);
    if(renderer == NULL)
    {
        std::cout << "Failed to create renderer! Error:" << SDL_GetError() << std::endl;
        exit(1);
    }

    SDL_Event event;
    bool running = true;

    std::cout << "Press \"r\" to reset \n\"q\" to quit\n";

    while(running)
    {
        SDL_SetRenderDrawColor(renderer,0,0,0,0); //clear with black
        SDL_RenderClear(renderer);

        while(SDL_PollEvent(&event))
        {

            if(turn && !won)
            {
                makeMove();
                turn = !turn;
            }

            switch(event.type)
            {
                case SDL_KEYDOWN:
                    if(event.key.keysym.sym == SDLK_q)
                        running = false;
                    else              
                    if(event.key.keysym.sym == SDLK_r)
                    {
                        for(short i = 0; i < 9; i++)
                        {
                            array[i] = EMPTY;
                        } 
                        turn = false;
                        won = false;
                    }
                break;

                case SDL_MOUSEMOTION:
                    SDL_GetMouseState(&mouseX, &mouseY);
                break;

                case SDL_MOUSEBUTTONDOWN:
                {
                    //skip if computer's turn or game finished
                    if(turn || won)
                        break;

                    short int position = 0;
                    position += (mouseX - 10) / 100 + 3 * ((mouseY - 10) / 100);
                    if(array[position] == EMPTY)
                    {
                        array[position] = PLAYER; 
                        turn = !turn;
                    }
                }
                break;
            }
        }

        drawGame(renderer);
        SDL_RenderPresent(renderer);
        SDL_Delay(10);
    }

    SDL_DestroyWindow(window);

    SDL_Quit();

    return 0;
}