#OBJS - which files to compile
OBJS = tictactoe.cpp

#OBJ_NAME - executable name
OBJ_NAME = ttt

#CC - which compiler to use
CC = g++

#COMPILER_FLAGS
COMPILER_FLAGS = -Wall -Werror -I. -I/usr/local/lib/ -I/home/bbojor/hdd/Downloads/SDL2-2.0.12/include/

#LINKER_FLAGS
LINKER_FLAGS =-L/usr/lib -L/usr/local/lib -Wl,-rpath,/usr/local/lib -Wl,--enable-new-dtags -lSDL2 -Wl,--no-undefined -lm -ldl -lts -lpthread -lrt

# compile command
all :   $(OBJS)
		$(CC) $(OBJS) $(COMPILER_FLAGS) -o $(OBJ_NAME) $(LINKER_FLAGS) 